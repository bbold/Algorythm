//
//  PlaylistDetailViewController.swift
//  Algorythm
//
//  Created by Mo Lotfi on 02/03/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//

import UIKit

class PlaylistDetailViewController: UIViewController {

    var playList: Playlist?
    
    @IBOutlet weak var playlistCoverImage: UIImageView!
    @IBOutlet weak var playlistTitle: UILabel!
    @IBOutlet weak var playlistDescription: UILabel!
    
    @IBOutlet weak var playlistArtist0: UILabel!
    @IBOutlet weak var playlistArtist1: UILabel!
    @IBOutlet weak var playlistArtist2: UILabel!
    @IBOutlet weak var playlistArtist3: UILabel!
    @IBOutlet weak var playlistArtist4: UILabel!
    @IBOutlet weak var playlistArtist5: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if playList != nil {
        
            playlistCoverImage.image = playList!.largeIcon
            playlistCoverImage.backgroundColor = playList!.backgroundColor
            playlistTitle.text = playList!.title
            playlistDescription.text = playList!.description
            playlistArtist0.text = playList!.artist[0]
            playlistArtist1.text = playList!.artist[1]
            playlistArtist2.text = playList!.artist[2]
            playlistArtist3.text = playList!.artist[3]
            playlistArtist4.text = playList!.artist[4]
            playlistArtist5.text = playList!.artist[5]

            
            
            
            
            
            
            
        }
        
        
        
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}
